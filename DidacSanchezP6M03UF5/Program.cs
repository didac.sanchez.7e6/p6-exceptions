﻿using System;
using System.IO;
using System.Collections.Generic;

namespace DidacSanchezP6M03UF5
{
    public class MutableList<T>
    {
        List<T> Mutable { get; set; }
        public MutableList()
        {
            Mutable = new List<T> { };
        }
        public void Afaxir(List<T> element)
        {
            try
            {
                Mutable.AddRange(element);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        public void Esborar(T element)
        {
            try
            {
                Mutable.Remove(element);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        public void Petar(List<T> element, T thing)
        {
            try
            {
               Console.WriteLine( Mutable.FindIndex(9, element => element.Equals(thing)));
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
            }

        }

    }
    internal class Program
    {
        static void Main()
        {
            TestDivisio();
            TestPass();
            Console.WriteLine(Read());
            TestMutableList();
        }

        /*--- Divicio ---*/
        public static int Divisio(int divident, int divisor)
        {

            try
            {
                return divident / divisor;
            }
            catch (ArithmeticException e)
            {
                Console.WriteLine(e);
                return 0;
            }
        }


        /*--- Pasar a Double ---*/
        public static double Pass(string combertir)
        {
            try
            {
                return double.Parse(combertir);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return 0;
            }
        }
        /*--- Llegir un document ---*/
        public static string Read()
        {
            try
            {
                string path = @"E:\Didac Sanchez Cullell - DAMv\M03\Ficheros\Files\wordcount.txt";
                StreamReader sr = File.OpenText(path);
                string document = sr.ReadToEnd();
                return document;
            }
            catch (IOException e)
            {
                return e.ToString();
            }
        }
        /*--- Lista ---*/


        /*--- Joc de probes ---*/

        public static void TestDivisio()
        {
            Console.WriteLine(Divisio(7, 2));
            Console.WriteLine(Divisio(8, 4));
            Console.WriteLine(Divisio(5, 0));
        }
        public static void TestPass()
        {
            Console.WriteLine(Pass("7,1"));
            Console.WriteLine(Pass("9"));
            Console.WriteLine(Pass(",2"));
            Console.WriteLine(Pass("tres"));
        }
        public static void TestMutableList()
        {
            MutableList<string> test = new MutableList<string>();
            test.Afaxir(null);
            test.Esborar(null);
            test.Petar(new List<string> { null }, default);
        }
    }
}
